package eu.specsproject.utility.credentialclient;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.google.gson.Gson;

public class CredentialClientTest extends Mockito{
    private static  ComponentInterfaceImplement interfaceImplement;
    private static HashMap<String, String> readCredentials;
    
	@BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("setUpBeforeClass Called");
        Secret s= new Secret();
        s.setId("Broker");
        s.setS("token_test");
        EntityManagerFactory managerFactory = new EntityManagerFactoryInstance().getEntityManagerFactory("Segreto");
        EntityManager em =  managerFactory.createEntityManager();
        EntityTransaction t = em.getTransaction();
        t.begin();
        //em.persist(s);
        //t.commit();
        //em.close();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        //do nothing
    }


    @Before
    public void setUp() throws Exception {
        System.out.println("setUp Called");
        interfaceImplement = new ComponentInterfaceImplement();
        ComponentManager.setComponentManager(interfaceImplement);
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("tearDown Called");
    }

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8200);
    
    @Test
    public final void testSecretEntity() {
        Secret secret = new Secret();
        Assert.assertNotNull(secret);
        
        secret.setId("id_test");
        Assert.assertEquals(secret.getId(), "id_test");

        secret.setS("string_test");
        Assert.assertEquals(secret.getS(), "string_test");
    }
    
    @Test
    public final void testSetComponentManager() {
        ComponentManager.setComponentManager(new ComponentInterfaceImplement());
        ComponentManager man = ComponentManager.getComponentManager();
        Assert.assertNotNull(man);
    }
    
    @Test
    public final void testComponentInterface() {
        ComponentInterface interf = new ComponentInterface<HashMap<String, String>>() {

            @Override
            public void setCredential(HashMap<String, String> a) {                
            }

        };
        Assert.assertNotNull(interf);
    }
    
    @Test
    public final void testComponentActionsReceiveToken() throws IOException{
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);       
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        when(mockRequest.getParameter("key")).thenReturn("key_test");
        when(mockRequest.getParameter("token")).thenReturn("token_test");
        
        ComponentActions action = new ComponentActions();
        HttpServletResponse response = action.receiveToken(mockRequest, mockResponse);
        Assert.assertEquals(0, response.getStatus());
    }
    
    @Test
    public final void testComponentActionsReceiveEucalyptusCredentials(){
        
        stubFor(get(urlEqualTo("/v1/secret/owner/Broker/IeatCluster/ChefOrganizationPrivateKey"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody("{\"auth\": null,"
                                + "\"data\": {\"value\": \"ChefOrganizationPrivateKey\"},"
                                + "\"lease_duration\": 2592000,"
                                + "\"lease_id\": \"\","
                                + "\"renewable\": false}")));
            
        stubFor(get(urlEqualTo("/v1/secret/owner/Broker/IeatCluster/ChefUserPrivateKey"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody("{\"auth\": null,"
                                + "\"data\": {\"value\": \"ChefUserPrivateKey\"},"
                                + "\"lease_duration\": 2592000,"
                                + "\"lease_id\": \"\","
                                + "\"renewable\": false}")));
        
        stubFor(get(urlEqualTo("/v1/secret/owner/Broker/IeatCluster/ProviderUserName"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody("{\"auth\": null,"
                                + "\"data\": {\"value\": \"ProviderUserName\"},"
                                + "\"lease_duration\": 2592000,"
                                + "\"lease_id\": \"\","
                                + "\"renewable\": false}")));
        
        stubFor(get(urlEqualTo("/v1/secret/owner/Broker/IeatCluster/ProviderUserPassword"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody("{\"auth\": null,"
                                + "\"data\": {\"value\": \"ProviderUserPassword\"},"
                                + "\"lease_duration\": 2592000,"
                                + "\"lease_id\": \"\","
                                + "\"renewable\": false}")));
        
        stubFor(get(urlEqualTo("/v1/secret/owner/Broker/IeatCluster/ProviderUserPrivateKey"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody("{\"auth\": null,"
                                + "\"data\": {\"value\": \"ProviderUserPrivateKey\"},"
                                + "\"lease_duration\": 2592000,"
                                + "\"lease_id\": \"\","
                                + "\"renewable\": false}")));
        
        ComponentActions action = new ComponentActions();
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);       
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        
        when(mockRequest.getParameter("Type")).thenReturn("eucalyptus");
        try {
            PrintWriter writer = new PrintWriter("test_file.txt");
            when(mockResponse.getWriter()).thenReturn(writer);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        try {
            HttpServletResponse response = action.receiveCredentials(mockRequest, mockResponse);
            Assert.assertEquals(0, response.getStatus());
            Assert.assertEquals(5, readCredentials.size());
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail("receiveCredentials method failed");
        }
    }
    
    @Test
    public final void testComponentActionsReceiveAmazonCredentials(){
        
        stubFor(get(urlEqualTo("/v1/secret/owner/Broker/C1"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody("{\"auth\": null,"
                                + "\"data\": {\"value\": \"ChefOrganizationPrivateKey\"},"
                                + "\"lease_duration\": 2592000,"
                                + "\"lease_id\": \"\","
                                + "\"renewable\": false}")));
            
        stubFor(get(urlEqualTo("/v1/secret/owner/Broker/C2"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody("{\"auth\": null,"
                                + "\"data\": {\"value\": \"ChefUserPrivateKey\"},"
                                + "\"lease_duration\": 2592000,"
                                + "\"lease_id\": \"\","
                                + "\"renewable\": false}")));
        
        stubFor(get(urlEqualTo("/v1/secret/owner/Broker/Cn"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody("{\"auth\": null,"
                                + "\"data\": {\"value\": \"ProviderUserName\"},"
                                + "\"lease_duration\": 2592000,"
                                + "\"lease_id\": \"\","
                                + "\"renewable\": false}")));
        
        ComponentActions action = new ComponentActions();
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);       
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        
        when(mockRequest.getParameter("Type")).thenReturn("amazon");
        try {
            PrintWriter writer = new PrintWriter("test_file.txt");
            when(mockResponse.getWriter()).thenReturn(writer);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        try {
            HttpServletResponse response = action.receiveCredentials(mockRequest, mockResponse);
            Assert.assertEquals(0, response.getStatus());
            Assert.assertEquals(3, readCredentials.size());
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail("receiveCredentials method failed");
        }
    }
    
    @Test
    public final void testWriteSecret(){
        
        stubFor(post(urlEqualTo("/writesecrettest"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody("true")));
        
        String response = VaultApi.WriteSecret("http://127.0.0.1:8200/writesecrettest", "token_test", "Value", "Description");
        
        Assert.assertEquals("200", response);
    }
    
    @Test
    public final void testTrigger(){
        
        stubFor(post(urlEqualTo("/triggertest"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody("true")));
        
        try {
            String response = VaultApi.trigger("http://127.0.0.1:8200/triggertest", "path_pair_test");
            Assert.assertEquals("true", response);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
    
    public class ComponentInterfaceImplement implements ComponentInterface<HashMap<String, String>>{

        @Override
        public void setCredential(HashMap<String, String> a) {
            // TODO Auto-generated method stub
            readCredentials = a;
            System.out.println("SET CREDENTIAL: "+a);
        }
        
    }
	
}
