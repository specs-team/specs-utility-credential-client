package eu.specsproject.utility.credentialclient;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * classe per salvare il token ricevuto in memory
 * @author giancarlo
 *
 */

public class EntityManagerFactoryInstance {

    private static EntityManagerFactory managerFactory;

    public EntityManagerFactory getEntityManagerFactory (String entityName){
        return managerFactory == null ? managerFactory = Persistence.createEntityManagerFactory(entityName): managerFactory;
    }
}
