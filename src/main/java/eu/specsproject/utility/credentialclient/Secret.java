package eu.specsproject.utility.credentialclient;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Segreto
 *
 */
@Entity

public class Secret implements Serializable {

	
	private String S;   
	@Id
	private String Id;
	private static final long serialVersionUID = 1L;

	public Secret() {
		super();
	}   
	public String getS() {
		return this.S;
	}

	public void setS(String S) {
		this.S = S;
	}   
	public String getId() {
		return this.Id;
	}

	public void setId(String Id) {
		this.Id = Id;
	}
   
}
