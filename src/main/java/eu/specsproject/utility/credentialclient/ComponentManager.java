package eu.specsproject.utility.credentialclient;

import java.util.HashMap;

/**
 * manager che gestisce il singleton dell'interfaccia che viene implementata
 * @author giancarlo
 *
 */
public class ComponentManager {
    private static ComponentManager man=null;
    private ComponentInterface<HashMap<String, String>> compInterface;
    
    
    private ComponentManager(ComponentInterface<HashMap<String, String>> a){
        setCompInterface(a);
    }
    
    public static synchronized ComponentManager setComponentManager(ComponentInterface<HashMap<String, String>> a){
    	if(man==null){ man=new ComponentManager(a);}
    	return man;
    }
    
    public static synchronized ComponentManager getComponentManager(){
    	return man;
    }
    
    public ComponentInterface<HashMap<String, String>> getCompInterface() {
        return compInterface;
    }
    
    private void setCompInterface(ComponentInterface<HashMap<String, String>> compInterface) {
        this.compInterface = compInterface;
    }
    
}
