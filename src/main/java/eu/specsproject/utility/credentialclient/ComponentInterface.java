package eu.specsproject.utility.credentialclient;

/**
 * interfaccia da implementare per far si che il componente possa ricevere le credenziali dal 
 * client
 * @author giancarlo
 *
 * @param <T>
 */

public interface ComponentInterface<T> {
	public void setCredential(T a);
}
