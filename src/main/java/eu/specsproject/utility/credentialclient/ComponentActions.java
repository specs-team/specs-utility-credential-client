package eu.specsproject.utility.credentialclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;


/**
 * il componente usa questa classe per salvare il token e fare il retrieve delle credenziali
 * @author giancarlo
 *
 */
public class ComponentActions {
    public static HttpServletResponse receiveToken(HttpServletRequest req, HttpServletResponse resp) throws IOException{
        String key=req.getParameter("key").toString();
        String token=req.getParameter("token").toString();
        System.out.println("Recive token, parameters: "+key+" "+token);
        Properties prop=new Properties();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader (ComponentActions.class.getClassLoader().getResourceAsStream("vault_conf.properties"), "UTF8"));
            prop.load(br);
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }  

        Secret s= new Secret();
        s.setId(key);
        s.setS(token);
        EntityManagerFactory managerFactory = new EntityManagerFactoryInstance().getEntityManagerFactory("Segreto");
        EntityManager em =  managerFactory.createEntityManager();
        EntityTransaction t = em.getTransaction();
        t.begin();
        em.persist(s);
        t.commit();
        em.close();
        resp.setStatus(200);
        System.out.println("aaaaaaaaaa"+s.getS());
        PrintWriter writer = resp.getWriter();
        writer.print("{ \"message\" : \"token received\"");
        return resp;

    }
    
    public static HttpServletResponse receiveCredentials(HttpServletRequest req, HttpServletResponse resp) throws IOException{
        String parameters = req.getParameter("Type").toString();
        System.out.println("Post, parameter returned: "+parameters);
        Properties prop=new Properties();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader (ComponentActions.class.getClassLoader().getResourceAsStream("vault_conf.properties"), "UTF8"));
            prop.load(br);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }/*
		/* I created a class named main that implements interface just writing on standard input all the EucalyptusCredentials values
         * after i put it into the ComponentManager to test it.
         * main n=new main();
		ComponentManager comp=ComponentManager.setComponentManager(n);*/
        /*EucalyptusCredentials a= new EucalyptusCredentials();
		ArrayList<String> Secret=null;

		Secret=VaultApi.ReadSecret(prop.get("vault_secret_path").toString()+prop.get("vault_component_path").toString()+"ChefOrganizationPrivateKey", "token");
		a.setChefOrganizationPrivateKey(Secret.get(1));
		Secret=VaultApi.ReadSecret(prop.get("vault_secret_path").toString()+prop.get("vault_component_path").toString()+"ChefUserPrivateKey", "token");
		a.setChefUserPrivateKey(Secret.get(1));
		Secret=VaultApi.ReadSecret(prop.get("vault_secret_path").toString()+prop.get("vault_component_path").toString()+"ProviderUserName", "token");
		a.setProviderUserName(Secret.get(1));
		Secret=VaultApi.ReadSecret(prop.get("vault_secret_path").toString()+prop.get("vault_component_path").toString()+"ProviderUserPassword", "token");
		a.setProviderUserPassword(Secret.get(1));
		Secret=VaultApi.ReadSecret(prop.get("vault_secret_path").toString()+prop.get("vault_component_path").toString()+"ProviderUserPrivateKey", "token");
		a.setProviderUserPrivateKey(Secret.get(1));
		/*a.setChefOrganizationPrivateKey("cheforganization");
		a.setChefUserPrivateKey("chefuserpk");
		a.setProviderUserName("pun");
		a.setProviderUserPassword("pup");
		a.setProviderUserPrivateKey("pup");*/
        /* ComponentManager c= ComponentManager.getComponentManager();
		c.inter.setCredential(a);*/
        EntityManagerFactory managerFactory = new EntityManagerFactoryInstance().getEntityManagerFactory("Segreto");
        System.out.println("VAULT COMPONENT NAME TO SEARCH: "+prop.get("vault_component_name").toString());
        Secret s = managerFactory.createEntityManager().find(Secret.class,prop.get("vault_component_name").toString());
        System.out.println("COMPONENT VALUES "+s.getId()+" "+s.getS());
        PrintWriter writer = resp.getWriter();
        HashMap<String,String> hash = new HashMap<String,String>();
        List<ClassLoader> classLoadersList = new LinkedList<ClassLoader>();
		classLoadersList.add(ClasspathHelper.contextClassLoader());
		classLoadersList.add(ClasspathHelper.staticClassLoader());
		Set<String> packages = findAllPackagesStartingWith("eu.specs.datamodel.credentials.");
		Iterator iterator2 = packages.iterator();
		int i=0;
		while (iterator2.hasNext()){
			String currentPackage = iterator2.next().toString();
			System.out.println("*****************");
			System.out.println("PACKAGE: "+currentPackage);
			Reflections reflections = new Reflections(new ConfigurationBuilder()
			.setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner())
			.setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
			.filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(currentPackage))));
			Set<Class<?>> classes = reflections.getSubTypesOf(Object.class);
			Iterator iterator = classes.iterator();
			String[] tempString= currentPackage.split("\\.");
			String componentName=tempString[tempString.length-1];
			while (iterator.hasNext()){
				String currentClass=iterator.next().toString(); 
				//
				String current=null;
				Class<?> c=null;
				String[] tempproviderName=null;
				String providerName=null;
				try {
					current = currentClass.substring(6);
					c = Class.forName(current);
					tempproviderName=current.split("\\.");
					providerName=tempproviderName[tempproviderName.length-1];
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Method[] tempp = c.getDeclaredMethods();
				List<Method> temp=Arrays.asList(tempp);
				Iterator iter= temp.iterator();
				while(iter.hasNext()){
					String field=iter.next().toString();
					String[] cutfields=field.split("\\.");
					String cutfield=cutfields[cutfields.length-1];
					if(cutfield.startsWith("get")){
						String realfields= cutfield.substring(3);
						String[] splitting= realfields.split("\\(");
					    String realfield=splitting[0];
					    realfield=realfield.substring(0, 1).toLowerCase()+realfield.substring(1,realfield.length());
					System.out.println(realfield);
					if(componentName.equals(prop.get("vault_component_name").toString())){
						if(providerName.equals(parameters)){
							String temp2= VaultApi.ReadSecret(prop.get("vault_secret_path").toString()+prop.get("vault_component_path").toString()+providerName+"/"+realfield, s.getS()).get(1);
							if(temp2 != null){
							hash.put(realfield,temp2);
							writer.print(temp2);
							}
						}
					}
					}
				}
				//
				System.out.println("classes: "+currentClass);
				//listofcredentials.add(iterator.next().toString());
				
				
				
				
			}
			i++;
			System.out.println("*****************");
		}
		//        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(SomeAnnotation.class);
		MockUp n=new MockUp();
		ComponentManager.setComponentManager(n);
        ComponentManager.getComponentManager().getCompInterface().setCredential(hash);
        if(hash!=null){
        	resp.setStatus(200);
        } else { resp.setStatus(400); }
        return resp;
    }
    public static Set<String> findAllPackagesStartingWith(String prefix) {
		List<ClassLoader> classLoadersList = new LinkedList<ClassLoader>();
		classLoadersList.add(ClasspathHelper.contextClassLoader());
		classLoadersList.add(ClasspathHelper.staticClassLoader());
		Reflections reflections = new Reflections(new ConfigurationBuilder()
		.setScanners(new SubTypesScanner(false), new ResourcesScanner())
		.setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
		.filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(prefix))));
		Set<Class<? extends Object>> classes = reflections.getSubTypesOf(Object.class);

		Set<String> packageNameSet = new TreeSet<String>();
		for (Class classInstance : classes) {
			String packageName = classInstance.getPackage().getName();
			if (packageName.startsWith(prefix)) {
				packageNameSet.add(packageName);
			}
		}
		return packageNameSet;
	}

}
