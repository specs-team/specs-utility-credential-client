package eu.specsproject.utility.credentialclient;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.*;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * classe che implementa le api necessarie a parlare con il vault server via HTTP
 * @author giancarlo
 *
 */

public class VaultApi {
	public String Token;
	public static String search(String a,String b) throws ClientProtocolException { 
		JSONParser parser = new JSONParser();
		KeyFinder finder = new KeyFinder();
		finder.setMatchKey(b);
		while(!finder.isEnd()){
			try {
				parser.parse(a, finder,true);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				throw new ClientProtocolException("500");
			}
			if(finder.isFound()){
				finder.setFound(false);
				System.out.println("found id:");
				System.out.println(finder.getValue());
				return finder.getValue().toString();
			}
		}
		return " ";
	}
	public static String doG(String path,String token) throws ClientProtocolException, IOException{
		CloseableHttpClient httpclient = HttpClients.createDefault();
		//HttpGet httpget= new HttpGet("http://127.0.0.1:8200/v1/"+path);
		HttpGet httpget= new HttpGet(path);
		if(!token.equals("")){
			httpget.addHeader("X-Vault-Token",token);
			//httpget.addHeader("X-Vault-Token","02e70312-638c-e6cc-aeeb-a10fd2ebd5a0");
		}
		System.out.println("Executing request " + httpget.getRequestLine());
		ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

			@Override
			public String handleResponse(
					final HttpResponse response) throws ClientProtocolException, IOException {
				int status = response.getStatusLine().getStatusCode();
				if (status >= 200 && status < 300) {
					System.out.println(status);
					HttpEntity entity = response.getEntity();
					//modificare la return per essere in linea con tutto
							return entity != null ? EntityUtils.toString(entity) : null;
							//return entity != null ? (Integer.toString(status)+"\n"+EntityUtils.toString(entity)) : Integer.toString(status);
				} else {
					throw new ClientProtocolException(Integer.toString(status));
					//return Integer.toString(status);
				}
			}

		};
		String responseBody = httpclient.execute(httpget, responseHandler);
		System.out.println("----------------------------------------");
		System.out.println(responseBody);

		{
			httpclient.close();
			return responseBody;
		}

	}
	public static String doP(String path,String token,String body,String application) throws ClientProtocolException, IOException{
		CloseableHttpClient httpclient = HttpClients.createDefault();
		//HttpPost httppost= new HttpPost("http://127.0.0.1:8200/v1/"+path);
		HttpPost httppost= new HttpPost(path);
		if(!token.equals("")){
			//System.out.println("non ci dovevo entrare");
			//httppost.addHeader("X-Vault-Token",token);
			httppost.addHeader("X-Vault-Token","02e70312-638c-e6cc-aeeb-a10fd2ebd5a0");
		}
		if(!body.equals("")){
			System.out.println("ci dovevo entrare");
			HttpEntity entity = new ByteArrayEntity(body.getBytes("UTF-8"));
			httppost.setEntity(entity);
			/*List <NameValuePair> nvps = new ArrayList <NameValuePair>();
			nvps.add(new BasicNameValuePair("key", "lol"));
			nvps.add(new BasicNameValuePair("token", "asd"));
			httppost.setEntity(new UrlEncodedFormEntity(nvps));*/
		}
		if(!application.equals("")){
			//System.out.println("non ci dovevo entrare");
			httppost.addHeader("Content-type",application);
		}
		System.out.println("Executing request " + httppost.getRequestLine());
		ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

			@Override
			public String handleResponse(
					final HttpResponse response) throws ClientProtocolException, IOException {
				int status = response.getStatusLine().getStatusCode();
				if (status >= 200 && status < 300) {
					HttpEntity entity = response.getEntity();
					return entity != null ? EntityUtils.toString(entity) : null;
					//return entity != null ? (Integer.toString(status)+"\n"+EntityUtils.toString(entity)) : Integer.toString(status);
				} else {
					//return Integer.toString(status);
					throw new ClientProtocolException(Integer.toString(status));
				}
			}

		};
		String responseBody = httpclient.execute(httppost, responseHandler);
		System.out.println("----------------------------------------");
		System.out.println(responseBody);
		httpclient.close();
		return responseBody;

	}
	public  void doAuth(String user,String pass) throws ClientProtocolException, IOException{
		String lol=doP("auth/userpass/login/"+user,"",pass,"");
		this.Token = this.search(lol,"client_token");
		//this.Token = this.search(lol,"client_token");	
	}
	public static ArrayList<String> ReadSecret(String path,String token) {
		String lol=null;
		ArrayList<String> values = new ArrayList<String>();
		try {
			lol = doG(path,token);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			values.add(e.getMessage());
			values.add(null);
			values.add(null);
			return values;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//return search(lol,"value");
		String value;
		try {
			System.out.println("Response Do Get ReadSecret: "+lol);
			value = search(lol,"value");
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			values.add(e.getMessage());
			values.add(null);
			values.add(null);
			return values;
		}
		String desc;
		try {
			desc = search(lol,"description");
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			values.add(e.getMessage());
			values.add(null);
			values.add(null);
			return values;
		}
		values.add("200");
		values.add(value);
		values.add(desc);
		return values;

	}
	public static String WriteSecret(String path,String token,String s,String desc) {
		//String lol=doP("secret/"+path,token,"{ \"password\": \"prova\" }","application/json");
		JSONObject p= new JSONObject();
		p.put("description", desc);
		p.put("value", s);
		//String lol=doP(path,token,"{ \"value\": \""+s+"\" }","application/json");
		String lol=null;
		try {
			doP(path,token,p.toJSONString(),"application/json");
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			lol=e.getMessage();
			e.printStackTrace();
			return lol;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lol="200";
		return lol;
	}
	public static String trigger(String component,String name) throws ClientProtocolException, IOException{
		CloseableHttpClient httpclient = HttpClients.createDefault();
		//HttpPost httppost= new HttpPost("http://127.0.0.1:8200/v1/"+path);
		HttpPost httppost= new HttpPost(component);
		List <NameValuePair> nvps = new ArrayList <NameValuePair>();
		//esempio add token
		nvps.add(new BasicNameValuePair("path", name));
		//esempio write secret
		//nvps.add(new BasicNameValuePair("path", "yolo"));
		//nvps.add(new BasicNameValuePair("secret", "SEGRETOSEGRETISSIMO"));
		try {
			httppost.setEntity(new UrlEncodedFormEntity(nvps));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Executing request " + httppost.getRequestLine());
		ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

			@Override
			public String handleResponse(
					final HttpResponse response) throws ClientProtocolException, IOException {
				int status = response.getStatusLine().getStatusCode();
				if (status >= 200 && status < 300) {
					HttpEntity entity = response.getEntity();
					return entity != null ? EntityUtils.toString(entity) : null;
				} else {
					throw new ClientProtocolException("Unexpected response status: " + status);
				}
			}

		};
		String responseBody = httpclient.execute(httppost, responseHandler);
		System.out.println("----------------------------------------");
		System.out.println(responseBody);
		httpclient.close();
		return responseBody;

	}



}