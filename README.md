# README #

This README would normally document whatever steps are necessary to get your Credential Client up and running.

### What do you need to use the Credential Client? ###

* You need to import the following dependencies in your IDE:
    * Data Model Project (https://bitbucket.org/specs-team/specs-utility-data-model)
    * Specs Utility parent Project (https://bitbucket.org/specs-team/specs-utility-specs_parent) 
* You need to convert the project to a Maven project (if your IDE doesn't recognize it as a Maven one);
* Execute Maven Install. 

### How to use the credential client? ###

* In order to use this component, it's important to import the credential client as a dependency in your web project. This can be easily achieved adding it as a Maven dependency. 
* The next step is to create a new class in you project that extends the following interface: "ComponentInterface.java" so that it can receive the credentials retrieved from the Vault Server.

### Who do I talk to? ###

* Massimiliano Rak (Cerict) - maxrak@gmail.com